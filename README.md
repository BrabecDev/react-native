# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Repositório de projetos e estudos de React Native

## Starting Points #

Começando um projeto com a plataforma expo
https://expo.io/learn

## How do I get set up? ##

### Linux - Ubuntu Systems ###

Instalando o nodejs
https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions

Instalando o Android Studio
https://askubuntu.com/questions/634082/how-to-install-android-studio-on-ubuntu

### Windows ###
Certifique-se de que sua máquina possui Java JDK instalado, Node JS, Python 2.7 e caso não possua um dispositivo android
com modo desenvolvedor habilitado, instale o Android Studio e crie um dispositivo virtual, de preferência qualquer celular
com 5".
OBS: Caso esteja rodando a aplicação no emulador do Android Studio, certifique-se que o projeto está configurado para a versão 23 do android. 
Portanto o SDK do dispositivo instalado deve ser referente a essa versão.

Após instalar o node JS, baixe o react-native-cli digitando no terminal:

> npm install -g react-native-cli

## Setting up Environment Variables

Além disso, depois de instalar o JDK, vá em Iniciar > Exibir configurações avançadas do sistema > Avançado > Variáveis de Ambiente,
Clique em Novo e Digite "JAVA_HOME", logo abaixo insira o caminho onde se encontra a pasta do jdk em seu sistema,
e.g., "C:\Program Files\Java\jdk1.8.0_151". 

ANDROID_HOME = C:\Users\"nome do usuario"\AppData\Local\Android\Sdk

Em seguida clique em Editar a variável PATH, digitando o diretório "C:\Users\'NomeSeuUsuario'\AppData\Local\Android\Sdk\platform-tools"
e certifique-se de que este diretório existe em seu computador.

Garanta o Eslint como extensão do seu editor de texto escolhido, ele facilitará ao destacar erros de sintaxe referentes ao React Native.
ao entrar no caminho do projeto escolhido, abra um terminal e instale o seguinte preset de regras para o eslint funcionar:

> npm install --save-dev eslint-config-rallycoding

no seu editor de texto (no meu caso é o Visual Code) instale a extensão Eslint, e crie um arquivo no diretório raiz da aplicação
intitulado ".eslintrc" com o seguinte código:

{
	"extends": "rallycoding" 
}

# Projects

## Albums 

### Dependencies

Instale a biblioteca Axios:

> npm install --save axios

## TaskManager 

### Dependencies

Instale o Firebase:
> npm install --save firebase

Instale o FLow:

OBS: Instale a extensão do Flow Language Support no seu editor de texto escolhido.

Execute o seguinte comando, para configurar o compilador:

> npm install --save-dev babel-cli babel-preset-flow

No package.json adicione nos scripts:

"scripts": {
	"build": "babel src/ -d lib/",
    "prepublish": "npm run build"
}

E então execute:

> npm run prepublish

Configure o FLow:

> npm install --save-dev flow-bin

Execute o FLow:

Ao executar pela primeira vez,
> npm run flow init 

Após rodar flow com init pela primeira vez, execute:
> npm run flow

FLow
https://flow.org/en/docs/install/

FLow for VSCode
https://github.com/flowtype/flow-for-vscode

### Running the application

DICA: Para evitar bugs como o "Unable to load script from assets", após inicializar o projeto pelo cli do react-native, abra-o no Android Studio para que ele configure automáticamente as pastas e arquivos do bundle.

Após clonar o repositório, cada pasta refere-se a um projeto específico, e.g., "albums".
Abra um terminal no projeto desejado e com o dispositivo android conectado, seja ele físico ou virtual, digite

> react-native run-android

### Activating live reload

Pressione CTRL + M na tela do emulador.

How to enable live reload in react native on android
https://stackoverflow.com/questions/36933287/how-to-enable-live-reload-in-react-native-on-android

### Bugs and solutions ###

Symlink error
$ npm config set bin-links false
https://github.com/npm/npm/issues/9901

Couldn't start project on Android: Error running adb: ADB server didn't ACK
https://stackoverflow.com/questions/38512025/react-native-run-android-mismatch-version-adb-server-version#38706579

Cannot launch emulator on Linux (Ubuntu 15.10)
https://stackoverflow.com/questions/35911302/cannot-launch-emulator-on-linux-ubuntu-15-10#36625175

"Error: EACCES: permission denied, open '/home/user/.babel.json'"
https://stackoverflow.com/questions/31502990/react-native-connection-errors

Como depurar a aplicação num dispositivo móvel
https://stackoverflow.com/questions/29289304/how-do-you-debug-react-native

Failed sdk location not found
https://stackoverflow.com/questions/32634352/react-native-android-build-failed-sdk-location-not-found#32640154

Unable to load script from assets - Windows/Linux
https://stackoverflow.com/questions/44446523/unable-to-load-script-from-assets-index-android-bundle-on-windows#44476757

MSBuild Error - installing Build Tools for Windows using npm
https://github.com/felixrieseberg/windows-build-tools 

FLow Error
https://stackoverflow.com/questions/45183375/how-to-install-flow-type-correctly-for-react-native0-46#45696516

FLow Error - "throw new Error('Platform not supported.');"
https://stackoverflow.com/questions/39321393/platform-not-supported-when-running-npm-run-flow#39323442

"[eslint] Unexpected require(). (global-require)"
https://stackoverflow.com/questions/37487007/eslint-es6-redux-global-required-unexpected-require#37487154

### Contribution guidelines ###

Curso Udemy - The Complete React Native and Redux Course
https://www.udemy.com/the-complete-react-native-and-redux-course/learn/v4/

React Native Express
http://www.reactnativeexpress.com/

Getting Started - React Native
https://facebook.github.io/react-native/docs/getting-started.html

### Who do I talk to? ###

Estudantes da tecnologia React Native e Empresas interessadas em inserir uma framework excelente em construir aplicações
mobile nativas com React e Javascript.