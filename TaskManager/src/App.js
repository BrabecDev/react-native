import React, { Component } from 'react';
import { View } from 'react-native';
import firebase from 'firebase';
import { Header, Spinner, Button } from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component {
    state = { loggedIn: null };

    // Life cycle method to handle the hook with the firebase auth
    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyBla32EGW6oDJfU0nPOAFevfYokP7xmpqA',
            authDomain: 'authentication-7eec2.firebaseapp.com',
            databaseURL: 'https://authentication-7eec2.firebaseio.com',
            projectId: 'authentication-7eec2',
            storageBucket: '',
            messagingSenderId: '591540763166'
        });

        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
                this.setState({ loggedIn: true });
            } else {
                this.setState({ loggedIn: false });
            }
        });
    }

    renderContent() {
        switch (this.state.loggedIn) {
            case true:
                return (
                    <Button onPress={() => firebase.auth().signOut()}>
                        Log Out
                    </Button>
                );
            case false:
                return (<LoginForm />);
            default:
                return (<Spinner size='large' />);
        }
    }

    render() {
        return (
            <View>
                <Header headerText="Authentication" />
                {this.renderContent()}
                <LoginForm />
            </View>
        );
    }
}

export default App;
