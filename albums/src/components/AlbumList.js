import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import axios from 'axios';
import AlbumDetail from './AlbumDetail';

// Here's an example of a Class Component
class AlbumList extends Component {
    state = { albums: [] };

    componentWillMount() { // it'll be called before rendering.
        axios.get('http://rallycoding.herokuapp.com/api/music_albums')
        .then(response => this.setState({ albums: response.data }));
    }


    renderAlbums() {
        return this.state.albums.map(album => 
            <AlbumDetail key={album.title} album={album} />
        );
    }


    render() {
        console.log(this.state);

        return (
            // in order to make it scrollable
            <ScrollView> 
                {this.renderAlbums()}
            </ScrollView>
        );    
    }
}


export default AlbumList;
